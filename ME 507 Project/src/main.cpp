#include <Arduino.h>
#include <PrintStream.h>
#if (defined STM32L4xx || defined STM32F4xx)
    #include <STM32FreeRTOS.h>
#endif
#include "taskshare.h"
#include "taskqueue.h"
#include "HumidTempSense.h"


void setup() 
{
  Serial.begin (115200);
  Wire.begin();
  delay (2000);
  Serial << "Humidity Sensor Test incoming" << endl;
  // Create a task which prints a slightly disagreeable message
  xTaskCreate (HumiditynTempTask,
              "Read Temperature and Humidity",                      // Name for printouts
              2048,                            // Stack size
              NULL,                            // Parameter(s) for task fn.
              1,                               // Priority
              NULL); 
  #if (defined STM32L4xx || defined STM32F4xx)
        vTaskStartScheduler ();
    #endif    
  

}

void loop() {
  // put your main code here, to run repeatedly:
}