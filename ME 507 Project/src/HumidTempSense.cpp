#include "HumidTempSense.h"

void HumiditynTempTask (void* p_params)
{
    (void)p_params;                     // Shuts up a compiler warning
    SHTC3 mySHTC3 = SHTC3();              // Declare an instance of the SHTC3 class
    switch(mySHTC3.begin())
    {
        case SHTC3_Status_Nominal : Serial.print("Nominal\n"); break;
        case SHTC3_Status_Error : Serial.print("Error\n"); break;
        case SHTC3_Status_CRC_Fail : Serial.print("CRC Fail\n"); break;
        default : Serial.print("Unknown return code\n"); break;
    }

    for(;;)
    {
        SHTC3_Status_TypeDef result = mySHTC3.update();             // Call "update()" to command a measurement, wait for measurement to complete, and update the RH and T members of the object

        if(mySHTC3.lastStatus == SHTC3_Status_Nominal)              // You can also assess the status of the last command by checking the ".lastStatus" member of the object
        {
            Serial.print("RH = "); 
            Serial.print(mySHTC3.toPercent());                        // "toPercent" returns the percent humidity as a floating point number
            Serial.print("%, T = "); 
            Serial.print(mySHTC3.toDegF());                           // "toDegF" and "toDegC" return the temperature as a flaoting point number in deg F and deg C respectively 
            Serial.println(" deg F"); 
        }
        else
        {
        Serial.print("Update failed, error\n"); 
        
        }
    }


}


          
  